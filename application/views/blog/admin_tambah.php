<body>
<div class="sidebar">
  <nav>
    <div class="asd">
      <div class="profil">
        <img src="https://cdn.shopify.com/s/files/1/0099/9562/files/Header-Icon-User.png" alt="">
        <span>Hi, admin</span>
      </div>
    </div>
    <ul>
      <li>
        <a href="<?=site_url('blog/admin')?>">Beranda</a>
      </li>
      <li>
        <a href="<?=site_url('blog/do_tambah')?>">Tambah Artikel </a>
      </li>
    </ul>
  </nav>
</div>
<div class="container">
  <div class="top">
    <div class="log">
      <a href="<?=site_url('blog')?>">Log out</a>
    </div>
  </div>
  <div class="content">
    <h1>Tambah Artikel</h1>
    <form>
      <div class="form">
        <div class="form-group">
          <label for="njudul">Judul: </label>
          <input type="text" name="" value="">
        </div>
        <div class="form-group">
          <label for="gambar">gambar: </label>
          <input style="padding-left:0px;"type="file" name="gambar" value=""accept=".jpg, .jpeg, .png" multiple>
        </div>
        <div class="form-group">
          <label for="isi">Isi: </label>
          <textarea style="resize:none"name="isi" rows="4"></textarea>
        </div>
        <div class="form-group">
          <div style="width:90%; margin-left:auto;margin-right:auto;">
            <button class="btn" type="button" name="button">simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</body>
</html>
