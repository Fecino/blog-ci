<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
  }

  function index()
  {
      $this->load->view('layout/head');
      $this->load->view('layout/user_header');
      $this->load->view('blog/index');
      $this->load->view('layout/user_footer');
  }
  public function detail()
  {
      $this->load->view('layout/head');
      $this->load->view('layout/user_header');
      $this->load->view('blog/detail');
      $this->load->view('layout/user_footer');
  }
  public function login()
  {
      $this->load->view('layout/login_header');
      $this->load->view('blog/login');
  }
  public function admin()
  {
      $this->load->view('layout/admin_header');
      $this->load->view('blog/admin_index');
  }
  function do_tambah(){
      $this->load->view('layout/admin_header');
      $this->load->view('blog/admin_tambah');
  }
  public function admin_detail()
  {
      $this->load->view('layout/admin_header');
      $this->load->view('blog/admin_detail');
      // code...
  }

}
