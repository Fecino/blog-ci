<body>
<div class="sidebar">
  <nav>
    <div class="asd">
      <div class="profil">
        <img src="https://cdn.shopify.com/s/files/1/0099/9562/files/Header-Icon-User.png" alt="">
        <span>Hi, admin</span>
      </div>
    </div>
    <ul>
      <li>
        <a href="<?=site_url('blog/admin')?>">Beranda</a>
      </li>
      <li>
        <a href="<?=site_url('blog/do_tambah')?>">Tambah Artikel </a>
      </li>
    </ul>
  </nav>
</div>
<div class="container">
  <div class="top">
    <div class="log">
      <a href="<?=site_url('blog')?>">Log out</a>
    </div>
  </div>
  <div class="content">
    <div class="detail">
      <div class="judul1">
        <h1>Itsuno Sempat Ingin Racik DmC: Devil May Cry 2</h1>
      </div>
      <h5>diposting oleh Fecino, 26 september 2018</h5>
      <div class="img">
        <img src="http://jagatplay.com/wp-content/uploads/2017/07/dmc-600x338-1-600x338.jpg" alt="">
      </div>
      <div class="isi">
        <p>
          Sebuah seri yang memancing kontroversi, ini mungkin kesan yang melekat pada DmC: Devil May Cry. Diposisikan sebagai seri reboot yang lahir dari tangan developer berbeda – Ninja Theory, keputusan untuk mengubah desain karakter Dante yang kini memiliki rambut hitam dan dandanan yang lebih realistis di kala itu, langsung memicu reaksi negatif. Padahal dari sisi gameplay, ia terasa jauh lebih intuitif dengan gaya yang tidak kalah kerennya dengan seri original Devil May Cry itu sendiri. Namun terlepas dari semua kritik yang mengemuka tersebut, Capcom sendiri sebenarnya suka dengan DmC: Devil May Cry tersebut. Pujian tersebut bahkan meluncur dari mulut sang otak franchise ini – Hideaki Itsuno.
        </p>
        <p>
          Dalam wawancaranya dengan VG247, Itsuno bahkan secara terbuka menyebut bahwa ia sempat tertarik untuk masuk dan menangani sebuah seri sekuel – DmC: Devil May Cry 2. Namun sulit diwujudkan, mereka akhirnya memutuskan banting setir dan berujung melahirkan Devil May Cry 5. Mengingat bahwa tim-nya saat ini setengahnya terdiri dari orang yang sempat menangani DmC dan setengahnya lagi tidak, maka ia memutuskan untuk membuat Devil May Cry 5 tetap berlanjut, namun belajar dari elemen yang berhasil diterapkan di DmC itu sendiri. Ambisinya tetap menciptakan sebuah seri yang berhasil melampaui kualitas game racikan Capcom dan Ninja Theory itu sendiri.
        </p>
        <figure class="img">
          <img src="http://jagatplay.com/wp-content/uploads/2018/09/dmc1-1024x640.jpg" alt="">
          <figcaption>Itsuno mengaku sempat tertarik untuk melanjutkan DmC: Devil May Cry.</figcaption>
        </figure>
        <p>
          Itsuno juga menyebut bahwa agak tidak masuk akal jika tiba-tiba Capcom menangani DmC: Devil May Cry 2 sendiri. Sejak awal, game ini adalah proyek kolaborasi dengan Ninja Theory. Jika mereka memutuskan untuk melakukan segala sesuatunya di Jepang, ia yakin bahwa seri sekuel ini akan kehilangan sesuatu yang menarik dan penting. Oleh karena itu, lebih rasional jika mereka melanjutkan Devil May Cry 4.
        </p>
        <p>
          Devil May Cry 5 sendiri rencananya akan dirilis pada tanggal 8 Maret 2019 mendatang untuk Playstation 4, Xbox One, dan tentu saja – PC. Bagaimana dengan Anda sendiri? Jika harus memilih antara DmC: Devil May Cry 2 atau Devil May Cry 5, manakah yang lebih Anda ingin muncul di pasaran?
        </p>
        <span>Sumber : <a href="http://jagatplay.com/2018/09/news/itsuno-sempat-ingin-racik-dmc-devil-may-cry-2/">JagatPlay</a>
        </span>
      </div>
    </div>
  </div>
</div>
</body>
</html>
