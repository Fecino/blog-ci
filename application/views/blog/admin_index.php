<body>
<div class="sidebar">
  <nav>
    <div class="asd">
      <div class="profil">
        <img src="https://cdn.shopify.com/s/files/1/0099/9562/files/Header-Icon-User.png" alt="">
        <span>Hi, admin</span>
      </div>
    </div>
    <ul>
      <li>
        <a href="<?=site_url('blog/admin')?>">Beranda</a>
      </li>
      <li>
        <a href="<?=site_url('blog/do_tambah')?>">Tambah Artikel </a>
      </li>
    </ul>
  </nav>
</div>
<div class="container">
  <div class="top">
    <div class="log">
      <a href="<?=site_url('blog')?>">Log out</a>
    </div>
  </div>
  <div class="content">
    <h1>Daftar Artikel</h1>
    <table>
      <thead>
        <tr>
          <th class="no">No.</th>
          <th class="judul">Judul</th>
          <th class="ringkasan">Isi</th>
          <th class="Gambar">Gambar</th>
          <th class="opsi"colspan="2">Opsi </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td><a href="<?=site_url('blog/admin_detail')?>">Itsuno Sempat Ingin Racik DmC:...</a></td>
          <td>Sebuah seri yang memancing kontroversi, ini mungkin kesan yang melekat pada DmC: Devil May Cry. Diposisikan sebagai seri reboot...</td>
          <td><a href="http://jagatplay.com/wp-content/uploads/2017/07/dmc-600x338-1-600x338.jpg" target="_blank">Gambar1.jpg</a> & <a href="http://jagatplay.com/wp-content/uploads/2018/09/dmc1-1024x640.jpg" target="_blank">Gambar2.jpg</a>
          </td>
          <td><a href="<?=site_url('blog/do_tambah')?>">edit</a></td>
          <td><a href="delete.html">delete</a></td>
        </tr>
        <tr>
          <td>2</td>
          <td><a href="<?=site_url('blog/admin_detail')?>">Red Dead Redemption 2 Butuh...</a></td>
          <td>Dunia luas terbuka dengan kurang lebih 200 spesies binatang hidup di dalamnya dengan sifat dan tingkah laku uniknya sendiri, detail...</td>
          <td><a href="http://jagatplay.com/wp-content/uploads/2017/07/dmc-600x338-1-600x338.jpg" target="_blank">Gambar1.jpg</a> & <a href="http://jagatplay.com/wp-content/uploads/2018/09/dmc1-1024x640.jpg" target="_blank">Gambar2.jpg</a>
          </td>
          <td><a href="<?=site_url('blog/do_tambah')?>">edit</a></td>
          <td><a href="delete.html">delete</a></td>
        </tr>
        <tr>
          <td>2</td>
          <td><a href="<?=site_url('blog/admin_detail')?>">Akhirnya, Sony Buka Cross-Pla...</a></td>
          <td>Seperti batu yang tidak bisa digerakkan, Sony sejauh ini memang terlihat tidak tertarik untuk mengimplementasikan konsep cross...</td>
          <td><a href="http://jagatplay.com/wp-content/uploads/2017/07/dmc-600x338-1-600x338.jpg" target="_blank">Gambar1.jpg</a> & <a href="http://jagatplay.com/wp-content/uploads/2018/09/dmc1-1024x640.jpg" target="_blank">Gambar2.jpg</a>
          </td>
          <td><a href="<?=site_url('blog/do_tambah')?>">edit</a></td>
          <td><a href="#">delete</a></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
</body>
</html>
